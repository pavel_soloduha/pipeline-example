pipeline {

	agent {
  		docker {
    		image 'alpine:3.7'
  		}
	}

	stages {
		stage('Find latest release branch') {
			steps {
				script {
					sh "apk add --no-cache git"
					
					sh "git branch -a"
					def versions = sh script: "git branch -a | grep -oE \"release-.*\" | grep -oE \"[[:digit:]]+\\.[[:digit:]]+\"", returnStdout: true
					
					println versions
					def vers = versions.split()
					
					def major_ver = 0
					def minor_ver = 0
					vers.each { release_version ->
						def nums = release_version.split('\\.')
						def tmp1 = nums[0] as Integer
						def tmp2 = nums[1] as Integer
						if (tmp1 > major_ver) {
							major_ver = tmp1
							minor_ver = tmp2
						} else if (tmp1 == major_ver && tmp2 > minor_ver) {
							minor_ver = tmp2
						}
					}

					println major_ver
					println minor_ver
					
					def version_choices = []
					version_choices  += "${major_ver}." + (minor_ver + 1)
					version_choices  += (major_ver + 1) + ".0"

					def version_input = input message: 'Choose release branch name to create', ok: 'Next',
                        parameters: [
                        	choice(name: 'release_version', choices: version_choices, description: 'Choose release branch name to create')
                    	]

                    withCredentials([usernamePassword(credentialsId: 'bitbucket_cred', passwordVariable: 'bitbucket_pass', usernameVariable: 'bitbucket_user')]) {
                    	sh "git config user.email \"${bitbucket_user}\""
                    	sh "git config user.name \"${bitbucket_user}\""
                    	
                    	sh "git checkout master"
                    	sh "git pull"
                    	sh "git checkout -b release-${version_input}"
                    	sh "git push --set-upstream origin release-${version_input}"
					}
				}
			}
		}
	}
}